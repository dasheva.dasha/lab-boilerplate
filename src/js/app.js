const crypto = require('crypto');
const {
  additionalUsers,
  randomUserMock,
} = require('./mock_for_L3.js');

/* eslint-disable no-console */

function normalize() {
  const map = new Map();
  randomUserMock.forEach((user) => {
    const normalisedUser = {
      gender: user.gender || null,
      title: user.name?.title || null,
      full_name: user.name ? `${user.name.first} ${user.name.last}` : null,
      city: user.location?.city || null,
      state: user.location?.state || null,
      country: user.location?.country || null,
      postcode: user.location?.postcode || null,
      coordinates: user.location?.coordinates || null,
      timezone: user.location?.timezone || null,
      email: user.email || null,
      b_date: user.dob?.date || null,
      age: user.dob?.age || null,
      phone: user.phone || null,
      picture_large: user.picture?.large || null,
      picture_thumbnail: user.picture?.thumbnail || null,
      id: user.id?.name && user.id?.value ? user.id.name + user.id.value : crypto.randomBytes(12)
        .toString('base64'),
      course: 'default course',
      bg_color: '#000000',
      note: 'person',
    };
    if (!map.has(normalisedUser.id)) {
      map.set(normalisedUser.id, normalisedUser);
    }
  });

  additionalUsers.forEach((user) => {
    if (!map.has(user.id)) {
      map.set(user.id, user);
    }
  });

  return Array.from(map.values());
}

const verifyParams = (params) => params.every((param) => typeof param === 'string' && param[0] === param[0].toUpperCase());
const verifyAge = (age) => typeof age === 'number' && !Number.isNaN(age);
const verifyEmail = (email) => /^\S+@\S+\.\S+$/.test(email);
const verifyPhone = (phone) => /^[+]?[(]?\d{3}[)]?[-\s.]?\d{3}[-\s.]?\d{4,6}$/.test(phone);

function isValidUser(user) {
  const paramsToVerify = [user.full_name, user.gender, user.note, user.state, user.city, user.country];
  return verifyParams(paramsToVerify)
        && verifyAge(user.age)
        && verifyEmail(user.email)
        && verifyPhone(user.phone);
}

function filterUsers(users, filter) {
  return users.filter((user) => {
    for (const [key, value] of Object.entries(filter)) {
      if (user[key] !== value) {
        return false;
      }
    }
    return true;
  });
}

function sortUsers(users, sort) {
  const compareFn = (user1, user2) => {
    for (const [param, direct] of Object.entries(sort)) {
      if (user1[param] < user2[param]) return direct * (-1);
      if (user1[param] > user2[param]) return direct;
    }
    return 0;
  };
  return users.sort(compareFn);
}

function findUser(users, match) {
  return users.find((user) => {
    let found = true;
    for (const [param, value] of Object.entries(match)) {
      if (user[param] === value) {
        found = false;
        break;
      }
    }
    return found;
  });
}

function userPercentage(users, research) {
  let count = 0;
  users.forEach((user) => {
    if (research(user)) count += 1;
  });
  // eslint-disable-next-line no-mixed-operators
  return count * 100 / users.length;
}

function test() {
  const normalized = normalize();
  console.log(isValidUser(normalized[0]));
  console.log(isValidUser(normalized[1]));
  console.log(filterUsers(normalized, { country: 'Australia', gender: 'female' }));
  console.log(sortUsers(normalized, { age: 1 }));
  console.log(findUser(normalized, { phone: '02-6397-0344' }));
  console.log(`${userPercentage(normalized, (user) => user.age > 30)}%`);
}

test();
